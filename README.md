# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Install Docker & Docker-compose
* Create Dockerfile & Docker-compose.yml
* Build, Start & Stop Using Docker-compose

### How do I get set up? ###

* "./docker-cli v" to view system details
* "./docker-cli init" to setup docker
* "./docker-cli config postgresql" to create postgresql docker-compose configuration
* "./docker-cli build postgresql" to build postgresql image
* "./docker-cli start postgresql" to start postgresql
* "./docker-cli top postgresql" to stop postgresql
* "./docker-cli config pgadmin" to create pgadmin docker-compose configuration
* "./docker-cli build pgadmin" to build pgadmin image
* "./docker-cli start pgadmin" to start pgadmin
* "./docker-cli top pgadmin" to stop pgadmin


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* shantanusk@hotmail.com